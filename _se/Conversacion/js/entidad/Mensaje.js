var Mensaje = function(texto){
    this.renderUsuarioNombres = true;
    this.texto = texto;
    this.fechaHuman = null;
    this.usuario = null;
    this.appended = false;
    this.visto = null;
    this.createNodes();
    this.adjuntos = [];
};

Mensaje.ids = [];
Mensaje.mensajes = {};

Mensaje.pushMensaje = function(mensaje)
{
    if(Mensaje.ids.indexOf(mensaje.id) < 0) {
        Mensaje.ids.push(mensaje.id);
        Mensaje.mensajes[mensaje.id] = mensaje;
    }
    return mensaje;
};
Mensaje.instancia = function(data)
{
    var mensaje = Mensaje.ids.indexOf(data.id) < 0 ? new Mensaje(data.texto) : Mensaje.mensajes[data.id];
    return Mensaje.pushMensaje(mensaje.addData(data));
};

Mensaje.prototype.addData = function(data)
{
    this.id = data.id;
    this.conversacion_id = data.conversacion_id;
    this.usuario_id = data.usuario_id;
    this.fecha = data.fecha;
    this.fechaHuman = data.fecha_human;
    this.hora = data.hora;
    this.texto = data.texto;
    this.adjunto = data.adjunto;
    this.visto = data.visto;
    this.usuario = data.usuario;
    this.adjuntos = data.adjuntos;
    return this;
};


Mensaje.prototype.createNodes = function()
{
    this.$cont = $('<div class="messaging-messages-line"></div>');
    
    this.$cont.append(
        '<article class="message message--sender">' +
            '<div class="u--arrange-fill message__message">' +
                '<div class="message__box"></div></div></article>');
    this.$box = this.$cont.find('.message__box');

    this.$nombres = $('<p class="message__name"></p>');
    this.$text = $('<div class="message__text"></div>');
    this.$time = $('<p class="message__time"></p>');
    this.$adjuntos = false;
    this.$box.append(this.$nombres, this.$text, this.$time);

    this.$visto = $('<span class="message__tick ch-icon-done-all"></span>');
    
    return this;
};

Mensaje.prototype.showFechaHuman = function()
{
    if(this.fechaHuman) 
        this.$cont.prepend('<div class="message-date"><small class="message-date__key">' + this.fechaHuman + '</small></div>');
    return this;
};
Mensaje.prototype.showNombres = function()
{
    if(this.usuario && !this.nombresAppended) {
        this.$nombres.append('<strong>' + this.usuario.nombres + '</strong>');
        this.$cont.find('article').removeClass('message--sender').addClass('message--receiver');
        this.vistoPrepended = true;
        this.nombresAppended = true;
    }
    return this;
};
Mensaje.prototype.showTexto = function()
{
    this.$text.empty().append(this.texto);
    this.showAdjuntos();
    return this;
};
Mensaje.prototype.showAdjuntos = function()
{
    if(this.adjuntos.length > 0 && !this.$adjuntos){
        this.$adjuntos = $('<div class="message__attachments"><ul></ul></div>');
        for(var i = 0 ; i < this.adjuntos.length; i++){
            var adjunto = this.adjuntos[i];
            this.$adjuntos.append(
                '<li class="message__attachments-item"> ' +
                    '<a href="'+adjunto.url+'" target="_blank"> ' +
                        '<span class="message__attachments-name u--truncate">'+adjunto.nombre+'</span> ' +
                    '</a> ' +
                    '<span class="u--text-light message__attachments-size">('+adjunto.size+')</span> ' +
                '</li>'
            );
        }
        this.$adjuntos.insertAfter(this.$text);
    }
};
Mensaje.prototype.showEnviando = function()
{
    this.showTexto().$text.append('<span class="message__status message__status--sending">Enviando...</span>');
    return this;
};
Mensaje.prototype.showError = function()
{
    this.showTexto().$text.append(
        '<span class="message__status message__status--fail">Hubo un error al enviar el mensaje. ' +
        '<a href="#" data-js="retry-link">Reintentar</a></span>'
    );
    return this;
};

Mensaje.prototype.showTime = function()
{
    if(this.hora)
        this.$time.append('<span class="message__timestamp">'+this.hora+'</span>');
    return this;
};

Mensaje.prototype.showVisto = function()
{
    if(this.visto !== null) {
        var cls = this.visto != 0 ? 'message_tick_read' : '';
        if (cls && !this.$visto.hasClass(cls))
            this.$visto.addClass(cls);
        if (!this.vistoPrepended) {
            this.vistoPrepended = true;
            this.$time.prepend(this.$visto);
        }
    }
    return this;
};


Mensaje.prototype.appendTo = function($parent, append)
{
    var appended = false;
    if(!this.appended){
        this.appended = appended = true;
        append ? $parent.append(this.$cont) : $parent.prepend(this.$cont);
    }
    return appended;
};

Mensaje.prototype.setAdjuntos = function(carchivo)
{
    for(var i in carchivo.adjuntos){
        
    }
}

