var CargarArchivo = function(chat, input_selector, btn_selector)
{
    var self = this;
    this.chat = chat;
    this.$inputAttach = $(input_selector);
    this.attachUrl = attach_url.replace('__cid__', this.chat.cid);

    this.$attachCont = this.chat.$wrap.find('.message-controls__files-container');
    this.adjuntos = {};
    this.uploading = false;

    if(typeof btn_selector != "undefined"){
        this.$btnAttach = $(btn_selector);
        this.$btnAttach.click(function(){
            self.openBrowser();
        })
    }
    this.$inputAttach.change(function(){
        var upload_files = self.$inputAttach[0].files;
        if(upload_files.length > 0){
            var files = [];
            $.each(upload_files, function(key, val){
                files.push(val);
            });
            if(files.length)
                self.uploadFiles(files);
        }
    });

    this.on = {};
};

CargarArchivo.prototype.openBrowser = function()
{
    this.$inputAttach.click();
};

CargarArchivo.prototype.uploadFiles = function(files)
{
    if(files.length){
        var file = files.shift();
        var $adjunto = this.renderAdjunto(file.name);
        this.uploadFile(file, $adjunto, function(){
            this.uploadFiles(files);
        })
    }else {
        this.uploading = false;
        this.en('uploadFiles');
    }
};

CargarArchivo.prototype.uploadFile = function(file, $adjunto, callback)
{
    var self = this;
    var formData = new FormData();
    formData.append("attach", file);

    var request = new XMLHttpRequest();
    request.open("POST", this.attachUrl);
    request.responseType = "json";
    request.onload = function(oEvent){
        if(this.response.ok){
            self.adjuntos[this.response.adjunto.id] = this.response.adjunto;
            self.updateAdjunto($adjunto, this.response.adjunto);
        }
        callback.call(self);
    };
    request.upload.addEventListener("progress", function(e) {
        var $progress = $adjunto.data('$progress');
        var pc = parseInt(e.loaded / e.total * 100);
        $progress[0].style.width = pc + "%";
        if(pc == 100) {
            $progress.addClass('progress__bar-success');
        }
    }, false);
    request.send(formData);
};

CargarArchivo.prototype.updateAdjunto = function($adjunto, adjunto)
{
    var self = this;
    if(!$adjunto.data('removed')) {
        this.$attachCont.show();
        $adjunto.find('button.ch-close').data('aid', adjunto.id);
        $('<span class="message-controls__file-size"> (<strong>' + adjunto.size + '</strong>)</span>').insertAfter($adjunto.find('.message__attachments-name'));
        setTimeout(function () {
            if(self.hasAdjuntos())
                $adjunto.data('$progress').closest('.progress').css('display', 'none');
        }, 1000);
        $adjunto.data('uploading', false);
    }
};

CargarArchivo.prototype.renderAdjunto = function(file_name)
{
    this.$attachCont.show();
    var $adjunto = $(
        '<li class="message-controls__file dz-processing dz-complete upload-success"> ' +
        '<span class="message__attachments-name u--truncate" data-dz-name="">'+file_name+'</span> ' +
        '<div class="progress" role="progressbar"> ' +
        '<div class="progress__bar" style="width: 0;"></div> ' +
        '</div> ' +
        '</li>'
    );
    var $eliminar = $('<button type="button" class="ch-close ch-icon-remove" aria-label="Close"></button>');
    $adjunto.append($eliminar);

    this.$attachCont.find('ul').append($adjunto);
    $adjunto.data('$progress', $adjunto.find('.progress__bar'));
    $adjunto.data('$progress').closest('.progress').css('display', 'inline-block');

    var self = this;
    $eliminar.click(function(){
        var $this = $(this);
        var $li = $this.closest('li');
        if(!$li.data('uploading'))
            delete self.adjuntos[$this.data('aid')];
        $this.closest('li').remove();
        $li.data('removed', true);
    });

    $adjunto.data('uploading', true);
    $adjunto.data('removed', false);

    return $adjunto;
};

CargarArchivo.prototype.limpiar = function()
{
    var adjuntos_ids = Object.keys(this.adjuntos);
    this.adjuntos = {};
    this.$attachCont.find('ul').empty();
    this.$attachCont.hide();
    return adjuntos_ids;
};

CargarArchivo.prototype.en = function(event, callback)
{
    if(typeof callback != "undefined"){
        if(typeof this.on[event] == "undefined")
            this.on[event] = [];
        this.on[event].push(callback);
    }else
    if(typeof this.on[event] != "undefined")
        for(var i = 0; i < this.on[event].length; i++)
            this.on[event][i](this);
};

CargarArchivo.prototype.hasAdjuntos = function()
{
    return Object.keys(this.adjuntos).length > 0;
};