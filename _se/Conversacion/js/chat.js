var Chat = function(cid, usuario_id)
{
    this.cid = cid;
    this.usuarioId = usuario_id;
    
    this.urlPost = '';
    this.urlRead = '';

    this.lastFechaHuman = '';
    this.mensajesNew = [];
    this.lastMensaje = null;
    this.interval = null;

    this.pageRender = 1;
    this.pageLast = 1;
    this.scroll = true;
};

Chat.prototype.setNodes = function($wrap)
{
    this.$wrap = $wrap;
    this.$box  = $wrap.find('.direct-chat-messages');
    this.$input = $wrap.find('.message-controls__text-input');
    this.$send = $wrap.find('button.enviar');
    this.$oldMensajesSpinner = $wrap.find('.btn-view-more--old-messages');
    this.$oldMensajesSpinner.hide();
    
    this.carchivo = new CargarArchivo(this, '.input-attach', '#attach-btn');

    var self = this;
    this.$send.click(function(e){
        e.preventDefault();
        if(self.carchivo.uploading){
            self.carchivo.en('uploadFiles', function(){
                self.enviar();
            })
        }else
            self.enviar();
    });
    this.$box.scroll(function(){
        if(self.scroll && self.pageRender < self.pageLast && self.$box[0].scrollTop == 0){
            self.$oldMensajesSpinner.show();
            self.scroll = false;
            self.pageRender++;
            var lastMensaje = self.lastMensaje;
            self.read(self.pageRender, function(){
                self.scroll = self.pageRender < self.pageLast;
                self.$box[0].scrollTop = lastMensaje.$cont[0].offsetTop;
                self.$oldMensajesSpinner.hide();
            });
        }
    });
};

Chat.prototype.setEditor = function(id)
{
    this.editor = new wysihtml5.Editor(id, {
        toolbar:        "toolbar",
        parserRules:    wysihtml5ParserRules,
        useLineBreaks:  false
    });
};

Chat.prototype.scrollDown = function()
{
    this.$box[0].scrollTop = this.$box[0].scrollHeight;
};

Chat.prototype.enviar = function()
{
    var texto = this.$input.val();
    if(texto || this.carchivo.hasAdjuntos()){
        var mensaje = new Mensaje(texto);
        this.mensajesNew.push(mensaje);
        this.showMensaje(mensaje.showEnviando());
        this.scrollDown();
        var self = this;
        $.post(this.urlPost, {'texto' : texto, 'adjuntos' : this.carchivo.limpiar()}, function(data){
            var mensaje = self.mensajesNew.shift();
            if(data.ok){
                self.showMensaje(Mensaje.pushMensaje(
                    mensaje.addData(data.mensaje).showTexto().showTime()
                ));
                self.scrollDown();
            }else{
                mensaje.showError();
            }
            self.$input.val('');
            self.editor.setValue('');
        });
    }
};

Chat.prototype.showMensaje = function(mensaje, append)
{
    append = typeof append == "undefined" || append;
    if(mensaje.fechaHuman && this.lastFechaHuman != mensaje.fechaHuman){
        this.lastFechaHuman = mensaje.fechaHuman;
        mensaje.showFechaHuman();
    }

    if(mensaje.usuario && mensaje.usuario.id != this.usuarioId) {
        mensaje.showNombres();
    }
    mensaje.showVisto();

    var appended = false;
    if(mensaje.appendTo(this.$box, append)) {
        mensaje.showTime();
        appended = true;
    }
    return appended;
};


Chat.prototype.read = function(page, callback)
{
    var self = this;
    page = typeof page == "undefined" ? 1 : page;
    $.get(this.urlRead+'?page='+page, function(data){
        if(data.ok){
            self.pageLast = parseInt(data.page_last);
            var show = false;
            for(var i = 0; i < data.mensajes.length; i++) {
                show = Mensaje.ids.indexOf(data.mensajes[i].id) == -1;
                var mensaje = Mensaje.instancia(data.mensajes[i]);
                if(show && self.showMensaje(mensaje.showTexto(), data.page == 1)) {
                    if(data.page == 1)
                        i == 0 ? self.lastMensaje = mensaje : null;
                    else
                        i+1 == data.mensajes.length ? self.lastMensaje = mensaje : null;
                }else
                    mensaje.showVisto();

            }

            if(typeof callback == "function")
                callback();

            if(show && data.page == 1)
                self.scrollDown();

            self.setInterval();
        }
    });
};

Chat.prototype.setInterval = function()
{
    var self = this;
    if(!this.interval) {
        this.interval = setInterval(function () {
            self.read();
        }, 5000);
    }
};



$(function(){
    $('.direct-chat').each(function(){
        var $this = $(this);
        var cid = $this.data('cid');
        if(cid) {
            var chat = new Chat(cid, usuario_id);
            chat.setNodes($this);
            chat.setEditor("editor");
            chat.urlPost = post_url.replace('__cid__', cid);
            chat.urlRead = read_url.replace('__cid__', cid);
            chat.read();
        }
    });
});