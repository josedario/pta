$(function(){
    if(jQuery().chosen)
        $('.chosen-select').chosen();
    if(jQuery().select2) {
        $('.select2').select2();
    }


    //form buscar por cliente
    var $form_buscar  = $('#form-buscar');
    var $select_buscar = $('select[name="buscar-cliente"]');
    $form_buscar.submit(function(){
        var cliente_id = $('select[name="buscar-cliente"]').val();
        if(cliente_id > 0){
            $form_buscar.attr('action', $form_buscar.attr('action').replace('__cid__', cliente_id));
            return true;
        }else if(cliente_id == null){
            $form_buscar.attr('action', $form_buscar.attr('action').replace('/__cid__', ''));
            return true;
        }
        return false;
    });
    $('input[name="limpiar-buscar"]').click(function(){
        $select_buscar.prop('value', null);
    })
    
});