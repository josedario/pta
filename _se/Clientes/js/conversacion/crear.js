var chat = new Chat(0, 8);
function enviar($texto, $asunto, $tipo, $tos)
{
    var ok = true;
    if(!$asunto.val()){
        $asunto.closest('.form-group').addClass('has-error').find('span.help-block').remove().end()
            .append('<span class="help-block">Por favor ingrese asunto</span>');
        ok = false;
    }else{
        $asunto.closest('.form-group').removeClass('has-error').find('span.help-block').remove();
    }
    if(!$texto.val()){
        $('iframe.wysihtml5-sandbox').addClass('has-error');
        $('.validation-emptyField').css('display', 'block');
        ok = false;
    }else{
        $('iframe.wysihtml5-sandbox').removeClass('has-error');
        $('.validation-emptyField').css('display', 'none');
    }

    if($tos.length > 0 && $tos.val().length == 0){
        $tos.closest('.form-group').addClass('has-error').find('span.help-block').remove().end()
            .append('<span class="help-block">Por favor ingrese destinatario</span>');
        ok = false;
    }else if($tos.length)
        $tos.closest('.form-group').removeClass('has-error').find('span.help-block').remove();
    
    if(ok){
        var $form = $('<form action="" method="post"></form>');
        $form.append('<input type="text" name="asunto" value="'+$asunto.val()+'" />');
        $form.append('<input type="text" name="texto" value="'+$texto.val()+'" />');
        $form.append('<input type="text" name="tipo" value="'+$tipo.val()+'" />');
        if($tos.length > 0)
            $.each($tos.val(), function(key, val){
                $form.append('<input type="text" name="tos[]" value="'+val+'" />');
            });
        var adjuntos = chat.carchivo.limpiar();
        for(var i = 0; i < adjuntos.length; i++)
            $form.append('<input type="text" name="adjuntos[]" value="'+adjuntos[i]+'" />');
        $('body').append($form);
        $form.submit();
    }
}
$(function(){
    chat.setEditor("editor");
    chat.$wrap = $('.direct-chat-primary');
    chat.carchivo = new CargarArchivo(chat, '.input-attach', '#attach-btn');


    var $send = $('button.enviar');
    var $input = $('.message-controls__text-input');
    var $asunto = $('#asunto');
    var $tipo = $('#tipo');
    var $tos = $('#tos');

    $send.click(function(e){
        e.preventDefault();
        if(chat.carchivo.uploading){
            chat.carchivo.en('uploadFiles', function(){
                enviar($input, $asunto, $tipo, $tos);
            })
        }else
            enviar($input, $asunto, $tipo, $tos);
    });
    
    $('.chosen').chosen();
});