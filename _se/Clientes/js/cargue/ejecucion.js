var autoliquidacionEjecucion = function(cliente_id, periodo){
    this.clieneId  = cliente_id;
    this.periodo   = periodo;
    this.ejecucionUrl = ajax_ejecucion_url;
};

autoliquidacionEjecucion.instances = {};
autoliquidacionEjecucion.instance = function(cliente_id, periodo)
{
    if(typeof autoliquidacionEjecucion.instances[cliente_id] == "undefined")
        autoliquidacionEjecucion.instances[cliente_id] = new autoliquidacionEjecucion(cliente_id, periodo);
    autoliquidacionEjecucion.instances[cliente_id].periodo = periodo;
    return autoliquidacionEjecucion.instances[cliente_id];
};


autoliquidacionEjecucion.prototype.ejecutar = function(callback)
{
    $.get(this.ejecucionUrl.replace('__cid__', this.clieneId).replace('__periodo__', this.periodo), function(data){
        var callback_arguments = [];
        if(data.ok){
            ClienteDOM.instance(data.cliente.id, data.cliente.label).getLiquidaciones();
            autoliquidacionEjecucion.stream();
        }else{
            callback_arguments = [data.exception, data.code];
        }
        typeof callback != "undefined" ? callback.apply(this, callback_arguments) : null;
    });
};
autoliquidacionEjecucion.enEjecucion = [];
autoliquidacionEjecucion.sourceOn = false;

autoliquidacionEjecucion.getCompleted = function(liquidaciones_ids)
{
    for(var i = 0; i < autoliquidacionEjecucion.enEjecucion.length; i++){
        var id = autoliquidacionEjecucion.enEjecucion[i];
        if(liquidaciones_ids.indexOf(id) < 0){
            autoliquidacionEjecucion.getLiquidacion(id);
        }
    }
    autoliquidacionEjecucion.enEjecucion = liquidaciones_ids;
};

autoliquidacionEjecucion.stream = function()
{
    if(!autoliquidacionEjecucion.sourceOn) {
        autoliquidacionEjecucion.sourceOn = true;
        var source = new EventSource(ajax_stream);

        source.addEventListener("liquidaciones", function (e) {
            var data = JSON.parse(e.data);
            var liquidaciones_ids = [];
            for (var i = 0; i < data.liquidaciones.length; i++) {
                var row = data.liquidaciones[i];
                liquidaciones_ids.push(row.id);
                var existe = ClienteDOM.instanceExiste(row.cliente.id);
                var cd = ClienteDOM.instance(row.cliente.id, row.cliente.label);
                if (existe)
                    cd.updateTabla(row);
            }
            autoliquidacionEjecucion.getCompleted(liquidaciones_ids);
        }, false);

        source.addEventListener("status", function (e) {
            var data = JSON.parse(e.data);
            console.log(data.message);
            //self.addStatus(data.message, "status");
            for (var i = 0; i < autoliquidacionEjecucion.enEjecucion.length; i++) {
                autoliquidacionEjecucion.getLiquidacion(autoliquidacionEjecucion.enEjecucion[i]);
            }
            autoliquidacionEjecucion.enEjecucion = [];
        });

        source.addEventListener("exception", function (e) {
            var data = JSON.parse(e.data);
            console.log(data.message);
            //self.addException(data.message, "exception");
            source.close();
        });

        source.onerror = function (e) {
            console.log('error');
            //self.addException("SSE ERROR", "onerror");
            source.close();
        };
    }
};



autoliquidacionEjecucion.getLiquidacion = function(liquidacion_id)
{
    $.get(ajax_get_liquidacion.replace('__id__', liquidacion_id), function(data){
        ClienteDOM.instance(data.cliente.id, data.cliente.label).updateTabla(data, true);
    })
};

autoliquidacionEjecucion.ejecutarMultiple = function(ids, periodo, callback)
{
    var id = ids.shift();
    var ae = autoliquidacionEjecucion.instance(id, periodo);
    ae.ejecutar(function (exception, code) {
        if(ids.length > 0 && (!exception || code == 1))
            autoliquidacionEjecucion.ejecutarMultiple(ids, periodo, callback);
        else
            callback(exception, code);
    });

};
    

$(function(){
    var $cliente_cargue = $('select[name="cliente_cargue"]');
    var $periodo = $('select[name="periodo"]');
    var $mensaje = $('#ejecucion-alert');
    var $modal_ejecucion = $('#modal-ejecucion');
    var clientes_ids = [];
    var chosen = false;
    
    $cliente_cargue.find('option').each(function(){
        var val = $(this).val();
        if(!isNaN(val) && val != 0)
            clientes_ids.push(val);
    });

    $modal_ejecucion.on('show.bs.modal', function(){
        if(clientes_ids.length > 1)
            $cliente_cargue.val(0);
        $periodo.val(0);
        $mensaje.empty();
        $mensaje.hide();
    });
    $modal_ejecucion.on('shown.bs.modal', function(){
        if(!chosen){
            $('.chosen-select-modal').chosen();
            chosen = true;
        }
    });
    $('#cargar-ejecucion').click(function(){
        var cliente_id = $cliente_cargue.val();
        var periodo = $periodo.val();
        if($cliente_cargue.val() != 0){
            var ids = $cliente_cargue.val() == "TODOS" ? clientes_ids.slice() : [cliente_id];
            if(ids.length > 0){
                $('#loading').show();
                autoliquidacionEjecucion.ejecutarMultiple(ids, periodo, function(exception, code){
                    if(!exception || $cliente_cargue.val() == "TODOS" && code == 1)
                        $modal_ejecucion.modal('hide');
                    else
                        $mensaje.empty().append(exception).show();
                    $('#loading').hide();
                })
            }
        }else{
            $mensaje.append('Ingrese valores').show();
        }
    });
});