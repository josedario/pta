$(function(){
    $('.chosen-select').chosen();
    
    //boton activar o desactivar configuracion
    var $show_loading = $('#loading');
    $('input[name=activo]').change(function(){
        var $this = $(this);
        var estado = $this.prop('checked') ? 1 : 0;
        $show_loading.css('display', 'block');
        $.get(ajax_activar.replace('__ccid__', $this.attr('value')).replace('__estado__', estado), function(data){
            $show_loading.css('display', 'none');
            if(!data.ok)
                alert(data.msj);
        });
    });

    //form buscar por cliente
    var $form_buscar  = $('#form-buscar');
    $form_buscar.submit(function(){
        var cliente_id = $('select[name="buscar-cliente"]').val();
        if(cliente_id > 0){
            $form_buscar.attr('action', $form_buscar.attr('action').replace('__cid__', cliente_id));
            return true;
        }else if(cliente_id == null){
            $form_buscar.attr('action', $form_buscar.attr('action').replace('/__cid__', ''));
            return true;
        }
        return false;
    });
    $('input[name="limpiar-buscar"]').click(function(){
        $('select[name="buscar-cliente"]').val(null);
    })
});