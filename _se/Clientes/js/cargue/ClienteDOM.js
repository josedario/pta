var ClienteDOM = function(cliente_id, cliente_label)
{
    this.clienteId = cliente_id;
    this.jsonUrl = json_url.replace('__cid__', this.clienteId);
    this.verEmpleadosUrl = ver_empleados_url;
    this.label = cliente_label;
    this.$box = null;
    this.$boxHeader = null;
    this.$boxBody = null;
    this.tabla = null;
    this.$barras = {};
    this.trsLiq = {};
    this.on = {};

    this.en('fillTabla', function(){
        $('span.cargue-volver-cargar').off('click').on('click', function(){
            var $this = $(this);
            autoliquidacionEjecucion.instance($this.data('cid'), $this.data('periodo')).ejecutar();
        });
        $('[data-toggle="tooltip"]').tooltip();
    })
};
ClienteDOM.instances = {};
ClienteDOM.instance = function(cliente_id, cliente_label)
{
    if(typeof ClienteDOM.instances[cliente_id] == "undefined") {
        var cd = new ClienteDOM(cliente_id, cliente_label);
        ClienteDOM.instances[cliente_id] = cd;
        cd.getLiquidaciones();
    }
    return ClienteDOM.instances[cliente_id];
};
ClienteDOM.instanceExiste = function(cliente_id)
{
    return typeof ClienteDOM.instances[cliente_id] != "undefined";
};

ClienteDOM.prototype.fillTabla = function(data)
{
    var self = this;

    if(!this.tabla) {
        this.tabla = new Tabla();
        this.tabla.appendTo(this.$boxBody);
        for(var data_name in data.tabla.cols)
            this.tabla.addTh(data.tabla.cols[data_name]);
        this.cols = data.tabla.cols;
        

    }
    this.tabla.removeRows().removePaginacion().setPaginacion(data.tabla.paginacion, function(page){
        self.trsLiq = {};
        self.getLiquidaciones(page);
    }).appendPaginacionTo(this.$boxHeader);

    for(var row_index = 0; row_index < data.tabla.rows.length; row_index++){
        var colspan = 0;
        var row = data.tabla.rows[row_index];
        for(var data_name in this.cols){
            this.tabla.addTd(row_index, this._fillTd(data_name, row));
            colspan++;
        }
        this.$barras[row.id] = new BarraProgreso(row.porcentaje_ejecucion);
        var td = $('<td colspan="'+colspan+'"></td>').append(this.$barras[row.id].$cont);
        var tr = $('<tr class="progreso"></tr>').append(td);
        this.tabla.$tbody.append(tr);
        this.trsLiq[row.id] = {"$tr" : this.tabla.getRow(row_index), "row_index" : row_index, "row" : row};
    }
    this.en('fillTabla');
};
ClienteDOM.prototype.updateTabla = function(liquidacion, force_update)
{
    force_update = typeof force_update != "undefined" ? force_update : false;
    if(typeof this.trsLiq[liquidacion.id] != "undefined"){
        var row = this.trsLiq[liquidacion.id].row;
        if(force_update || row.porcentaje_ejecucion != liquidacion.porcentaje_ejecucion) {
            this.trsLiq[liquidacion.id].row = liquidacion;
            var row_index = this.trsLiq[liquidacion.id].row_index;
            var col_index = 0;
            for (var data_name in this.cols) {
                var fill_td = this._fillTd(data_name, liquidacion);
                this.tabla.updateTd(row_index, col_index, fill_td);
                col_index++;
            }
            this.$barras[liquidacion.id].update(liquidacion.porcentaje_ejecucion);
            this.en('fillTabla');
        }
    }
};
ClienteDOM.prototype._fillTd = function(data_name, row)
{
    var ret = typeof row[data_name] != 'undefined' ? row[data_name] : '';
    var disabled = row.cargando ? "disabled" : "";
    switch(data_name){
        case 'empleados':
            ret = '<span class="btn btn-small btn-success empleados-exitosos" href="#"><i class="fa fa-user"></i> '+row.empleados_exitosos+'</span>' +
                '<span class="btn btn-small btn-danger empleados-no-exitosos" href="#"><i class="fa fa-user"></i> '+row.empleados_no_exitosos+'</span>';
            ret += '<a class="btn btn-small btn-default cargue-ver-empleados" href="' + this.verEmpleadosUrl.replace('__cid__', row.id) + '"><i class="fa fa-eye"</a>';
            break;
        case 'exportar':
            var disabled_exportar = disabled || row.empleados_exitosos == 0 ? 'disabled' : '';
            var tag = disabled_exportar == 'disabled' ? 'span' : 'button';
            ret = '<'+tag+' type="submit" name="zip" class="btn btn-small btn-default cargue-exportar-zip '+disabled_exportar+'" ' +
                (disabled ? '' : 'value="'+row.id+'"' ) +' data-toggle="tooltip" data-placement="top" title="Comprimido Zip"><i class="fa fa-file-archive-o"></i></'+tag+'>'
                + '<'+tag+' type="submit" name="pdf" class="btn btn-small btn-default cargue-exportar-uni '+disabled_exportar+'" ' +
                (disabled ? '' : 'value="'+row.id+'"' ) + ' data-toggle="tooltip" data-placement="top" title="PDF Unificado"><i class="fa fa-file-pdf-o"</'+tag+'>';
            break;
        case 'volver_cargar':
            var cls = row.cargando ? 'fa fa-refresh fa-spin fa-fw' : 'fa fa-repeat';
            var color = 'default';
            if(!row.cargando) {
                color = row.comando_estado == 3 ? 'danger' : 'success';
                cls   = disabled || row.empleados_no_exitosos > 0 ? cls : 'fa fa-check'
            }
            var span_cls = disabled || row.empleados_no_exitosos == 0 ? 'disabled' : 'cargue-volver-cargar';
            ret = '<span class="btn btn-small btn-'+color+' '+span_cls+' " data-cid="'+row.cliente.id+'" data-periodo="'+row.periodo+'">' +
                '<i class="'+cls+'"></i></span>';
            break;
        case 'email':
            var cls = row.email ? 'primary' : 'default';
            var tag = 'span';
            var attrs = '';
            if(!disabled) {
                tag = 'button';
                attrs = 'type="submit" name="email" value="' + row.id + '" ';
            }
            ret = '<'+tag+' '+attrs+' class="show-loading btn btn-small btn-'+cls+' '+disabled+'" ' +
                'data-toggle="tooltip" data-placement="top" title="Correo"><i class="fa fa-envelope"></i></'+tag+'>';
            break;
        case 'eliminar':
            var eliminar_disabled = row.cargando ? 'disabled' : '';
            ret = '<a class="btn btn-small btn-default cargue-eliminar '+eliminar_disabled+'" ' + 'data-toggle="tooltip" data-placement="top" title="Borrar"' +
                     'href="'+borrar_url.replace('__caid__', row.id)+'"><i class="fa fa-eraser"></i></a>';
            break;
    }
    return ret;
};

ClienteDOM.prototype.getLiquidaciones = function(page)
{
    var self = this;
    var url = typeof page !== "undefined" ? this.jsonUrl + "/"+page : this.jsonUrl;
    $.get(url, function(data){
        if(data.tabla.rows.length > 0){
            self.createBoxCliente();
            self.fillTabla(data);
        }
    })
};
ClienteDOM.prototype.createBoxCliente = function()
{
    if(!this.$box) {
        this.$box = $('<div class="box" class="cliente"></div>');
        this.$boxHeader = $('<div class="box-header with-border"><h5>' + this.label + '</h5></div>');
        this.$boxBody = $('<div class="box-body"></div>');
        this.$box.append(this.$boxHeader, this.$boxBody);
        $('#autoliquidaciones-clientes').append(this.$box);
    }
};
ClienteDOM.prototype.en = function(event, callback)
{
    if(typeof callback != "undefined"){
        if(typeof this.on[event] == "undefined")
            this.on[event] = [];
        this.on[event].push(callback);
    }else
    if(typeof this.on[event] != "undefined")
        for(var i = 0; i < this.on[event].length; i++)
            this.on[event][i](this);
};