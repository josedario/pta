var BarraProgreso = function(progreso){
    this.$cont = null;
    this.$barra = null;
    this.progreso = typeof progreso == "undefined" ? 0 : progreso;
    this.crearNode();
};
BarraProgreso.prototype.crearNode = function()
{
    this.$cont  = $('<div class="progress progress-xxs"></div>');
    this.$barra = 
        $('<div class="progress-bar progress-bar-success" role="progressbar" ' +
        'aria-valuenow="'+this.progreso+'" aria-valuemin="0" aria-valuemax="100" style="width: '+this.progreso+'%">' +
        '</div>');
    this.$cont.append(this.$barra);
};

BarraProgreso.prototype.update = function(progreso)
{
    this.$barra.attr('aria-valuenow', progreso).css('width', progreso+"%");
};