class Evaluacion {
    async _doAjax(ajaxUrl) {
        let result;

        try {
            result = await $.ajax({
                url: this.$el.data('ajax-url-' +  ajaxUrl),
                type: 'GET',
            });

            return result;
        } catch (error) {
            console.error(error);
        }
    }

    async _getDiapositiva()
    {
        const content = await this._doAjax('get-diapositiva');
        this.$el.html(content);
    }

    constructor($el) {
        this.$el = $el;
    }

    async init() {
        const status = await this._doAjax('status');
        if(status.continuar) {
            await this._getDiapositiva();
        } else {
            // TODO redireccionar status.redireccionarUrl
        }
    }
}

$(function(){
   $('.evaluacion-component').each(function() {
       const evaluacion = new Evaluacion($(this));
       evaluacion.init();
   })
});