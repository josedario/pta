$( function() {
    $('.todo-list').sortable({
        placeholder         : 'sort-highlight',
        handle              : '.handle',
        forcePlaceholderSize: true,
        zIndex              : 999999,
        update: function(event, ui) {
            $('.todo-list li').each( function(e) {
                const $li = $(this);
                const indice = $li.index() + 1;
                $li.find('input').val(indice);
                $li.find('.label.label-primary').html(indice);
            });
        }
    })
} );