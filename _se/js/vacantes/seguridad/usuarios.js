/**
 * Funcionalidades relacionadas a agregar usuario
 */
function agregar_usuario()
{
    var $agregar_submit = $('#modal-agregar-submit');
    $('.button-buscar').click(function(e){
        e.preventDefault();
        var ident = $('#ident').val();
        if(ident) {
            $.get(ajax_url.replace('__ident__', ident), function (data) {
                if (data) {
                    $('.nombres').text(data.primer_nombre+' '+data.primer_apellido);
                    $('.correo').text(data.email);
                    $('.info-wrap').removeClass('hidden');
                    $('.roles-wrap').removeClass('hidden');
                } else {
                    alert('la busqueda no dio resultados');
                }
            })
        }
    });
    $('#roles-select').change(function(){
        $(this).val() != 0 ? $agregar_submit.removeAttr('disabled') : $agregar_submit.attr('disabled', true);
    });
    $agregar_submit.click(function(){
        $('#form-agregar').submit();
    })
}
/**
 * Funcionalidades de editar usuario
 */
function editar_usuario()
{
    var $modal_editar = $('#modal-editar');
    var $roles_select = $('#roles-select-editar');
    var $agregar_submit = $('#modal-editar-submit');

    $modal_editar.on('show.bs.modal', function(e){
        var $relatedTarget = $(e.relatedTarget);
        var ident = $relatedTarget.data('ident');
        var rol   = $relatedTarget.data('rol');
        $('#ident-editar').val(ident);
        $roles_select.val(rol);
    });
    $roles_select.change(function(){
        $(this).val() != 0 ? $agregar_submit.removeAttr('disabled') : $agregar_submit.attr('disabled', true);
    });
    $agregar_submit.click(function(){
        $('#form-editar').submit();
    })
}
function borrar_usuario()
{
    $('a.borrar').click(function(){
        var ident = $(this).data('ident');
        var $form = $("<form action='' method='post'></form>");
        $form.append("<input type='hidden' name='borrar' value='"+ident+"'>");
        $('body').append($form);
        $form.submit();
    })
}
$(function(){
    agregar_usuario();
    editar_usuario();
    borrar_usuario();
});