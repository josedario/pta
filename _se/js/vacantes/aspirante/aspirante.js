$(function(){
    var $datepicker = $('.datepicker');
    if($datepicker.length > 0)
        $datepicker.each(function(){
            var $el = $(this);
            var end_date = 0;
            var $this = $(this);
            if($this.attr('id') === 'nacimiento' && !$this.hasClass('no-end-date')){
                end_date = "-14y";
            }
            $('.datepicker').datepicker({
                format: "yyyy-mm-dd",
                language: "es",
                startDate: "1920-01-01",
                endDate: end_date
            });
        }).mask("9999-99-99", {placeholder:"AAAA-MM-DD"});
    
    if($('.chosen-select').length > 0){
        $(".chosen-select").chosen({no_results_text: "No existe la opción!", width: "100%"});
    }

    //on change paises cambia select dptos y ciudades
    var paises = ['ident_pais_id', 'nac_pais_id', 'resi_pais_id', 'pais_id'];
    for(var i = 0; i < paises.length; i++) {
        $('#' + paises[i]).change(function() {
            var $pais = $(this);
            var pid = $pais.val();
            var $paisId = $pais.attr('id');
            var $departamento = $('#' + $paisId.replace('pais', 'dpto'));
            var $ciudad = $('#' + $paisId.replace('pais', 'ciudad'));

            $departamento.find('option').remove();
            $departamento.trigger("chosen:updated");
            $ciudad.find('option').remove();
            $ciudad.trigger("chosen:updated");

            if(pid !== 0) {
                $.get(url_json_dpto.replace('__pid__', pid), function(data) {
                    if(data.departamentos.length > 0) {
                        for(var j = 0; j < data.departamentos.length; j++) {
                            $departamento.append('<option value="'+data.departamentos[j].id+'">'+data.departamentos[j].nombre+'</option>');
                        }
                        $departamento.removeAttr('disabled');
                        $departamento.trigger("chosen:updated");
                    }
                    if(data.ciudades.length > 0) {
                        for (var j = 0; j < data.ciudades.length; j++) {
                            $ciudad.append('<option value="'+data.ciudades[j].id+'">'+data.ciudades[j].nombre+'</option>');
                        }
                        $ciudad.removeAttr('disabled');
                        $ciudad.trigger("chosen:updated");
                    }
                });
            }
        });
    }
    // on change dptos cambia ciudades
    var dptos = ['ident_dpto_id', 'nac_dpto_id', 'resi_dpto_id', 'dpto_id'];
    for(var i = 0; i < dptos.length; i++){
        $('#'+dptos[i]).change(function(e){
            var $departamento = $(this);
            var $depId = $departamento.attr('id');
            var did = $departamento.val();
            var pid = $('#'+$depId.replace('dpto', 'pais')).val();
            // vivienda no maneja pais
            if(!pid) {
                pid = '057';
            }
            var $ciudad = $('#'+$departamento.attr('id').replace('dpto', 'ciudad'));
            $ciudad.find('option').remove();
            $ciudad.trigger('chosen:updated');

            if(did !== 0){
                $.get(url_json_ciudad.replace('__pid__', pid).replace('__did__', did), function(data, status){
                    if(data.length > 0) {
                        for (var j = 0; j < data.length; j++) {
                            $ciudad.append('<option value="'+data[j].id+'">'+data[j].nombre+'</option>');
                        }
                        $ciudad.removeAttr('disabled');
                        $ciudad.trigger("chosen:updated");
                    }
                });
            }else{
                $ciudad.attr('disabled', 'disabled');
                $ciudad.trigger("chosen:updated");
            }
        });
    }

    $('#modalTabla').on('show.bs.modal', function(){
        $(".chosen-select").trigger("chosen:updated");
        $instituto.trigger('change');
    });

    var $instituto = $("#estudio_instituto_id.chosen-select");
    $instituto.change(function(){
        var val = $(this).val();
        var $nombre_alt_wrap = $('#institucion_nombre_alt_wrap');
        if(val == 0)
            $nombre_alt_wrap.show();
        else
            $nombre_alt_wrap.hide();
    });

    var $estatura = $('#estatura');
    var $peso = $('#peso');
    if($estatura.length > 0)
        $estatura.mask("9.99", {placeholder:"0.00"});
    if($peso.length > 0)
        $peso.mask("99.99", {placeholder: "00.00"});

    //estudio, check graduado uncheck cancelo y viceversa
    var $graduado = $('#graduado');
    var $cancelo  = $('#cancelo');
    $graduado.change(function(){
        if(this.checked)
            $cancelo.prop('checked', false);
    });
    $cancelo.change(function(){
        if(this.checked)
            $graduado.prop('checked', false);
    })
});