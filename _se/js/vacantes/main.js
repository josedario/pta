function portal_connect_print_msj(pid, msj)
{
    var wrap = $(".dropdown.login.portal-" + pid + " .panel-heading.msj");
    if(!wrap.length) {
        wrap = $('<div class="panel-heading msj warning"></div>');
        $(".dropdown.login.portal-" + pid + " .panel-heading").after(wrap)
    }
    wrap.text(msj);
}
$(function(){
    var msj = "";
    if(typeof window.open_dropdown != "undefined") {
        portal_connect_print_msj(window.open_dropdown, window.portal_connect_msj);
        $(".dropdown.login.portal-" + window.open_dropdown + " .dropdown-toggle").dropdown('toggle');
    }

    $("form.portal-connect").submit(function(e){
        var pid  = $(this).find("input[type=hidden]").val();
        var user = $(this).find("input[name='portal_user']").val();
        var pss  = $(this).find("input[name='portal_pss']").val();

        if(!user || !pss){
            portal_connect_print_msj(pid, 'Complete los campos');
            e.preventDefault();
        }
   })
});