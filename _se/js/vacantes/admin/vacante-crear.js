$(function(){
    $("#ciudad").chosen();
    $("#area").chosen();
    $("#cargo").chosen();

    $("#nivel_id").chosen({disable_search_threshold: 20});
    $("#subnivel_id").chosen({disable_search_threshold: 20});
    $("#contrato_tipo_id").chosen({disable_search_threshold: 20});
    $("#intensidad_horaria_id").chosen({disable_search_threshold: 20});
    $("#nivel_academico_id").chosen({disable_search_threshold: 20});
    $("#profesion").chosen({disable_search_threshold: 20});


    $("#descripcion").simplyCountable({
        counter   : '#descripcion-simply-countable',
        maxCount  : '2000',
        strictMax : true
    });

    $("#requisitos").simplyCountable({
        counter   : '#requisitos-simply-countable',
        maxCount  : '2000',
        strictMax : true
    });

    $("#titulo").simplyCountable({
        counter   : '#titulo-simply-countable',
        maxCount  : 100,
        strictMax : true
    });
    $("#titulo_complemento").simplyCountable({
        counter   : '#titulo-complemento-simply-countable',
        maxCount  : '100',
        strictMax : true
    });


    update_subniveles();

    $("#idioma_id").change(function(e){
        var val = $(e.target).val();
        if(!val) {
            var porcentaje_disabled = $("#idioma_porcentaje").attr('disabled');
            if(typeof porcentaje_disabled === typeof undefined || porcentaje_disabled === false)
                $("#idioma_porcentaje").attr('disabled', 'disabled').val($("#idioma_porcentaje option:first").val());
        }
        else
            $("#idioma_porcentaje").removeAttr('disabled');
    });

    $("#sector input").change(function(){
        sectorBehaviour($(this));
    });
    $("#sector input").each(function(){
       sectorBehaviour($(this));
    });
    sectorSelectedRender();


    $("form#vacante").submit(function(e){
        sectorSubmit();
    })

    $("input.no-licencia-conduccion").change(function(){
        var no_check = this;
        $("input.licencia_conduccion").each(function(){
            if(no_check.checked)
                this.checked = false;
        })
    })
    $("input.licencia_conduccion").change(function(){
        if(this.checked)
            $("input.no-licencia-conduccion")[0].checked = false;
        else if(!$("input.licencia_conduccion:checked").length)
            $("input.no-licencia-conduccion")[0].checked = true;
    })
});

function update_subniveles(){

    var nivel_id_no_changed = true;
    var subniveles = window.subniveles;
    var vacante_new = window.vacante_new;

    $("#nivel_id").change(function(){
        var nivel_id = $(this).val();
        if(nivel_id_no_changed && vacante_new){
            $(this).find("option:first").remove();
            $("#nivel_id").trigger("chosen:updated");
            $("#subnivel_id").removeAttr("disabled");
            nivel_id_no_changed = false;
        }
        $("#subnivel_id").empty();
        $.each(subniveles, function(key, obj){
            if(obj.nivel_id == nivel_id)
                $("#subnivel_id").append('<option value="'+obj.id+'">'+obj.name+'<option>');
        });
        $("#subnivel_id").trigger("chosen:updated");
    })
}

/**
 * Encierra todo el comportamiento de sector al ser seleccionado
 */
function sectorBehaviour($this)
{
    if($this.is(':checked')){
        $("#subsector .hider").empty();
        var subsectores = window.subsectores[$this.val()];
        for(var i in subsectores){
            $("#subsector .hider").append(subsectorCheckbox(subsectores[i].id, subsectores[i].name, subsectores[i].parent));
        }
    }
}
/**
 * pinta subsector y comportamiento al seleccionar o quitar
 * @param id
 * @param name
 * @param parent
 * @returns {jQuery|HTMLElement}
 */
function subsectorCheckbox(id, name, parent)
{
    var checked = typeof window.sector_selected[parent] != 'undefined' && 
                  typeof window.sector_selected[parent][id] != 'undefined' ? 'checked' : '';

    var checkbox = $('<div class="checkbox">'
        + '<label class="no-validation"><input type="checkbox" value="'+id+'" data-parent="'+parent+'" '+checked+'>'+name+'</label>'
        + '</div>');

    checkbox.find('input').change(function(){
        var $this = $(this);
        if($this.is(':checked')){
            var sector_selected = window.sectores[$this.data('parent')];
            if(typeof window.sector_selected[sector_selected.id] ===  'undefined')
                window.sector_selected[sector_selected.id] = {};

            window.sector_selected[sector_selected.id][$this.attr('value')] = $this.parent().text();
            sectorSelectedRender();
        }else{
            subsectorRemove($this.attr('value'), $this.data('parent'))
        }
    });
    return checkbox;
}
/**
 * elimina de la coleccion de sector-subsecor seleccionado
 * @param id
 * @param parent
 */
function subsectorRemove(id, parent)
{
    delete window.sector_selected[parent][id];
    var i = 0;
    for(var j in window.sector_selected[parent])
        i++;
    if(i == 0)
        delete window.sector_selected[parent];

    sectorSelectedRender();
}

/**
 * pinta el recuadro de sector-subsector seleccionado a partir de la coleccion window.sector_selected
 */
function sectorSelectedRender()
{
    $('#sector-sel table').empty();

    $('#subsector input').each(function(){
        $(this).prop("checked", false);
    });

    for(var sector_id in window.sector_selected){
        var sector_name_printed = false;
        for(var subsector_id in window.sector_selected[sector_id]){
            var sector_name = sector_name_printed ? '' : window.sectores[sector_id].name;
            var subsector_name = window.subsectores[sector_id][subsector_id].name;

            var $append = $('<tr><td>'+sector_name+'</td><td>'+subsector_name+'</td>' +
                '<td><i class="fa fa-times remove" data-sector="'+sector_id+'" data-subsector="'+subsector_id+'"></i></td></tr>');

            $append.find('i').click(function(){
                subsectorRemove($(this).data('subsector'), $(this).data('sector'));
            });

            $('#sector-sel table').append($append);
            sector_name_printed = true;

            $('#subsector input').each(function(){
                if($(this).attr('value') == subsector_id)
                    $(this).prop('checked', true);
            });
        }
    }
}

function sectorSubmit()
{
    $("#sector input, #subsector input").each(function(){
        $(this).prop("checked", false);
    });
    for(var sector_id in window.sector_selected) {
        $("form#vacante").append('<input type="hidden" name="sector[]" value="'+sector_id+'">');
        for (var subsector_id in window.sector_selected[sector_id]){
            $("form#vacante").append('<input type="hidden" name="sector[]" value="'+subsector_id+'" >');
        }
    }
}