$(function(){
    $(".show-loading").click(function(e){
        $("#loading").show();
    });
    $('[data-toggle="tooltip"]').tooltip();

    let $body = $('body');

    $body.on('expanded.pushMenu', function(){
        $.get(sidebar_menu_collapse_url.replace('__state__', 0), function(){
        });
    });

    $body.on('collapsed.pushMenu', function() {
        $.get(sidebar_menu_collapse_url.replace('__state__', 1), function(){
        });
    });

    // expanded.pushMenu
    // $pushMenu.close();
});