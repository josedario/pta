var ComandoSalida = function(json)
{
    this.comando_id = json.id;
    this.index = json.index;
    this.fecha = json.fecha;
    this.salida = json.salida;
    this.type = json.type;
};

ComandoSalida.prototype.getSalida = function()
{
    var salida = this.salida.replace(/(?:\r\n|\r|\n)/g, '<br />');
    return salida;
};