var comandoModalVer = function()
{
    var self = this;

    this.$modal = $('#modalVer');
    this.attributes = {'id' : null, 'namespace' : null, 'usuario' : null, 'fecha' : null, 'estado' : null, 'comando' : null};
    for(var i in this.attributes)
        this['$'+i] = $('#'+i);
    this.$terminalWrap = $('.terminal-wrap');
    this.$opcionesWrap = $('.opciones-wrap');
    this.$ejecutar = $('#ejecutar');
    this.$mensajes = this.$modal.find('.mensajes');

    this.ajaxOpciones = ajax_opciones;
    this.ajaxEjecutar = ajax_ejecutar;

    this.$ejecutar.click(function(){
        self.ejecutar();
    })

};

comandoModalVer.prototype.setProperty = function(name, value)
{
    if(typeof(this.attributes[name]) != "undefined") {
        this['$' + name].html(value);
        this.attributes[name] = value;
    }
};

comandoModalVer.prototype.terminalStream = function()
{
    this.$terminalWrap.empty();
    this.terminal = TerminalControlador.objetos[this.attributes.id];
    this.terminal.render(this.$terminalWrap);
};

comandoModalVer.prototype.terminalStreamClose = function()
{
    this.terminal.streamClose();
};
comandoModalVer.prototype.renderOpciones = function()
{
    var self = this;
    this.$opcionesWrap.empty();
    $.get(this.ajaxOpciones.replace('__cid__', this.attributes['id']), function(data){
        self.$comando.html(data.comando);
        for(var i = 0; i < data.opciones.length; i++)
            self.$opcionesWrap.append('<div class="col-sm-12"><div class="opcion">'+data.opciones[i].opcion+'</div></div>')
    })
};
comandoModalVer.prototype.ejecutar = function()
{
    this.terminalStreamClose();
    this.$terminalWrap.empty();
    var self = this;
    $.get(this.ajaxEjecutar.replace('__cid__', this.attributes['id']), function(data){
        if(data.status){
            self.terminalStream();
        }else{
            self.setMensaje(data.message);
        }
    })
};
comandoModalVer.prototype.setMensaje = function(mensaje)
{
    this.$mensajes.empty();
    this.$mensajes.append('<div class="col-md-12"><div class="alert alert-danger"> <p>'+mensaje+'</p></div> </div>');
};


$(function(){
    var modal = new comandoModalVer();
    modal.$modal.on('show.bs.modal', function(e){
        var $target = $(e.relatedTarget);
        $target.closest('tr').find('td').each(function(){
            var $this = $(this);
            modal.setProperty($this.attr('class'), $this.html());
        });
        modal.renderOpciones();
        modal.terminalStream();
    });

    modal.$modal.on('hide.bs.modal', function(e){
        modal.terminalStreamClose();
    });
});