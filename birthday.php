<html>
    <body style="font-family: 'Open Sans', Arial, sans-serif; color:white;">
    <table cellpadding="0" cellspacing="0" border="0" style="width: 874px; height:620px; margin:0 auto;">
        <tr>
            <td background="https://www.pta.com.co/img/birthday-background.png" bgcolor="#23527D" valign="top">
                <!--[if gte mso 9]>
                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;">
                    <v:fill type="tile" src="https://www.pta.com.co/img/birthday-background.png" color="#7bceeb" />
                    <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                <![endif]-->
                <div>
                    <h1 style="text-align: center; font-weight: bold; padding: 40px 0;">
                        ¡ HOLA Pepito mendieta !
                    </h1>
                    <h3 style="text-align: center; font-weight: normal;">

                    </h3>
                    <div class="text" style="padding: 20px; text-align: center; font-size: 1.5em;">
                        <p>
                            Hoy es un día de regocijo para nuestra empresa, estamos felices de compartir contigo un año más de tu vida.
                            Nos sentimos privilegiados al poder contar con tu presencia y es nuestro deseo que en éste día recibas el amor de tu familia, compañeros y amigos.
                        </p>
                    </div>
                </div>
                <!--[if gte mso 9]>
                </v:textbox>
                </v:rect>
                <![endif]-->
            </td>
        </tr>
    </table>
    <p style="font-size: 12px; color: #828584; text-align: center">
        <?php echo "pta www.pta.com.co" ?>.<br>
    </p>
    </body>
</html>