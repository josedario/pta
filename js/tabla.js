$(function(){
    var tabla = window.tabla;
    if(tabla){
        var $modal = $('#modalTabla');
        $modal.$title = $modal.find('.modal-title');

        var $modalBorrar = $('#modalTablaBorrar');
        $modalBorrar.$input = $modalBorrar.find('input[name=id]');

        tabla.limpiarModal = function()
        {
            $modal.find('.alert').remove();
            $modal.find('form input[type=text]').each(function(){
                $(this).val('');
            });
            $modal.find('form select').each(function(){
                $(this).val(0);
            });
            $modal.find('.has-error').removeClass('has-error');
            $modal.find('.help-block.error').remove();
            $modal.find('input[name="id"]').val('');
        };

        $modal.on('hidden.bs.modal', function(){
            tabla.limpiarModal();
            $modal.$title.text(tabla.modal_titulo);
        });

        if(tabla.modal_ver)
            $modal.modal('show');

        $('.tabla-editar').click(function(){
            $btn = $(this);
            tabla.limpiarModal();
            var entidad = tabla.entidades[$btn.data('entidad-index')];
            for(var attr in entidad){
                $input = $modal.find('form [name="'+attr+'"]');
                if($input.length)
                    $input.val(entidad[attr]);
            }
            $modal.$title.text(tabla.modal_titulo.replace('Agregar', 'Editar'));
            $modal.modal('show');
        });
        $('.tabla-eliminar').click(function(){
            $btn = $(this);
            var entidad = tabla.entidades[$btn.data('entidad-index')];
            $modalBorrar.$input.val(entidad.id);
            $modalBorrar.modal('show');
        });
    }
});