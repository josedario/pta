var ValidationCandidatos = function () {

    return {

        //Validation
        initValidation: function () {
            $("#candidatos-form").validate({
                // Rules for form validation
                rules:
                {
                    name:{
                        required: true
                    },
                    email:{
                        required: true,
                        email: true
                    },
                    tel : {
                        required: true,
                        digits: true
                    }
                },

                // Messages for form validation
                messages:
                {
                    name: {
                        required: 'Por favor ingrese su nombre'
                    },
                    email:{
                        required: 'Por favor ingrese un correo electronico',
                        email:    'Por favor ingrese correo electronico valido'
                    },
                    tel:
                    {
                        required: 'Ingrese un numero de telefono para contacto',
                        digits: 'Solo se permite el ingreso de numeros'
                    }
                },

                // Do not change code below
                errorPlacement: function(error, element)
                {
                    error.addClass('text-color-secondary').insertAfter(element);
                }
            });
        }

    };
}();

ValidationCandidatos.initValidation();

if(window.candidatos_form_submited){
    $("#formModal").modal('show');
}
